Links
=====

This repo is a coding challenge I took up, as a C coding exercise.

The pseudo code alogrithms comes from here:
https://www.toptal.com/developers/sorting-algorithms


Licensing
---------

This repo's content is released under the public domain.

Feel free to copy, modify and redistribute the code.

Read the UNLICENSE file for more infos.
