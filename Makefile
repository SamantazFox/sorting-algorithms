###########################
# Compiler flags
###########################

WARNFLAGS = -Wall -pedantic

CFLAGS   = $(WARNFLAGS) -std=c11
CXXFLAGS = $(WARNFLAGS) -std=c++11


###########################
# Targets
###########################

all: mergesort


rng/rng.o: rng/rng.cxx
	g++ ${CXXFLAGS} -c -o $@ $<

src/%.o: src/%.c
	gcc ${CFLAGS} -c -o $@ $<


%sort: rng/rng.o src/%.o
	g++ ${CXXFLAGS} -o $@ $^
