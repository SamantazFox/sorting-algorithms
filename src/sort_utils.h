#ifndef _SORT_UTILS_H_
#define _SORT_UTILS_H_


#include <stdio.h>


void printArray(const char* prefix, int* array, size_t len)
{
	printf("%s = { ", prefix);

	for (size_t i = 0; i < len-1; i++)
		printf("%d, ", array[i]);

	printf("%d }\n", array[len-1]);
}

void copyArray(int* dest, int* src, size_t len)
{
	for (size_t i = 0; i < len; i++)
		dest[i] = src[i];
}


#endif  /* !_SORT_UTILS_H_ */
