/*
 * Merge sort
 *
 * Properties:
 *
 *   - Stable
 *   - Θ(n) extra space for arrays (as shown)
 *   - Θ(lg(n)) extra space for linked lists
 *   - Θ(n·lg(n)) time
 *   - Not adaptive
 *   - Does not require random access to data
 *
 *
 * Algorithm:
 *
 *   # split in half
 *   m = n / 2
 *
 *   # recursive sorts
 *   sort a[1..m]
 *   sort a[m+1..n]
 *
 *   # merge sorted sub-arrays using temp array
 *   b = copy of a[1..m]
 *   i = 1, j = m+1, k = 1
 *   while i <= m and j <= n,
 *       a[k++] = (a[j] < b[i]) ? a[j++] : b[i++]
 *       → invariant: a[1..k] in final position
 *   while i <= m,
 *       a[k++] = b[i++]
 *       → invariant: a[1..k] in final position
 *
*/


#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "../rng/rng.hxx"
#include "sort_utils.h"


#define swapSort(a,b) { \
		if ((a) > (b)) { \
			__typeof__ (a) * x = (__typeof__ (a) *) malloc( sizeof(__typeof__ (a)) ); \
			if (x == NULL) exit (1); \
			*x = (a); (a) = (b); (b) = *x; \
			free(x); \
		} \
	}


void sort(int* a, size_t len)
{
	if (len == 2) swapSort(a[0], a[1]);
	if (len <= 2)	return;

	// split in half (or almost)
	size_t hlen = (int)  floor(len / 2);

	// recursive sorts
	sort(a, hlen);
	sort(a+hlen, len-hlen);

	// merge sorted sub-arrays using temp array
	int b[len];
	copyArray(b, a, len);

	size_t i = 0, j = hlen, k = 0;

	while (i < hlen && j < len)
		b[k++] = (a[i] < a[j]) ? a[i++] : a[j++];

	if      (i < hlen) while (i < hlen) b[k++] = a[i++];
	else if (j <  len) while (j <  len) b[k++] = a[j++];

	// Replace the result in a
	copyArray(a, b, len);
	return;
}


int main(void)
{
	// Init an array of random numbers
	size_t len = randomUInt64(10, 1500);
	int* array = (int*) randomUInt32Array(0, 1000, len);

	// Display unsorted, sort, display sorted
	printArray("\nbefore", array, len);
	sort(array, len);
	printArray("\nafter", array, len);

	// Don't forget to clean up
	free(array);
	return 0;
}
